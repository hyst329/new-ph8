#!/usr/bin/env python3
from enum import Enum
import sqlite3
import time
import random
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtSvg import *
from PyQt5.QtWidgets import *
from chessboard import *
from movegen import MoveGenerator


class GameMode(Enum):
    Waiting = 0
    Solving = 1
    Editing = 2


class PieceItem(QGraphicsSvgItem):
    renderers = {
        PIECE_WhiteKing: QSvgRenderer("pieces/wking.svg"),
        PIECE_WhiteQueen: QSvgRenderer("pieces/wqueen.svg"),
        PIECE_WhiteBishop: QSvgRenderer("pieces/wbishop.svg"),
        PIECE_WhiteRook: QSvgRenderer("pieces/wrook.svg"),
        PIECE_WhiteKnight: QSvgRenderer("pieces/wknight.svg"),
        PIECE_WhitePawn: QSvgRenderer("pieces/wpawn.svg"),
        PIECE_BlackKing: QSvgRenderer("pieces/bking.svg"),
        PIECE_BlackQueen: QSvgRenderer("pieces/bqueen.svg"),
        PIECE_BlackBishop: QSvgRenderer("pieces/bbishop.svg"),
        PIECE_BlackRook: QSvgRenderer("pieces/brook.svg"),
        PIECE_BlackKnight: QSvgRenderer("pieces/bknight.svg"),
        PIECE_BlackPawn: QSvgRenderer("pieces/bpawn.svg")
    }

    def __init__(self, piece, scene, parent=None):
        QGraphicsSvgItem.__init__(self, parent)
        self.setSharedRenderer(PieceItem.renderers[piece])
        self.setAcceptedMouseButtons(Qt.LeftButton)
        self.setFlags(QGraphicsItem.ItemIsSelectable |
                      QGraphicsItem.ItemIsMovable)
        self._dragged = False
        self.scene = scene
        self.piece = piece

    def mousePressEvent(self, event):
        if self.scene.mode != GameMode.Solving:
            QGraphicsItem.mouseReleaseEvent(self, event)
            return
        self.setCursor(Qt.ClosedHandCursor)
        QGraphicsItem.mousePressEvent(self, event)
        offset = self.scene.cellSize // 20
        self._from = ((self.sceneBoundingRect().center().x() - offset) // self.scene.cellSize,
                      (self.sceneBoundingRect().center().y() - offset) // self.scene.cellSize)

    def mouseMoveEvent(self, event):
        if self.scene.mode != GameMode.Solving:
            QGraphicsItem.mouseReleaseEvent(self, event)
            return
        self._dragged = True
        QGraphicsItem.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if self.scene.mode != GameMode.Solving:
            QGraphicsItem.mouseReleaseEvent(self, event)
            return
        offset = self.scene.cellSize // 20
        moved = False
        if self._dragged:
            pos = self.sceneBoundingRect().center()
            x, y = pos.x(), pos.y()
            x -= offset
            y -= offset
            i = x // self.scene.cellSize
            j = y // self.scene.cellSize
            _to = (i, j)
            move = "%s%s%s%s" % (chr(int(self._from[0]) + ord('a')),
                                 8 - int(self._from[1]),
                                 chr(int(i) + ord('a')),
                                 8 - int(j))
            if (self.piece == PIECE_WhitePawn and int(j) == 0
                    or self.piece == PIECE_BlackPawn and int(j) == 7):
                promote = ["Q = Queen", "R = Rook", "B = Bishop", "N = Knight"]
                piece, ok = QInputDialog.getItem(self.scene.form, "Promote to",
                                                 "Piece", promote, 0, False)
                self.setSharedRenderer(PieceItem.renderers[self.piece])
                if ok:
                    pstr = piece[0].lower()
                    move += pstr
                    self.piece = (-1, 1)[self.piece < 0] * \
                        " kqbnrp".index(pstr)
            print(move)
            moved = move in self.scene.gen.generateMoves(self.scene.board)
            cap = False
            if not moved:
                i, j = self._from
            else:
                cap = self.scene.board.move(move)
                self.scene.form.half_moves += 1

                if not self.scene.board.white_to_move:
                    movestr = "%d. %s-%s    " % (
                        self.scene.board.move_number, move[0:2], move[2:4])
                else:
                    movestr = "%s-%s\n" % (move[0:2], move[2:4])
                self.scene.form.moveView.insertPlainText(movestr)
            x = i * self.scene.cellSize
            y = j * self.scene.cellSize
            x += offset
            y += offset
            maybecap = [p for p in self.scene.items(QPointF(x, y))
                        if p != self]
            captured = maybecap[0] if maybecap else None
            print(captured)
            self.setPos(x, y)
            if moved and captured:
                self.scene.removeItem(captured)
            self._dragged = False
            if moved and self.scene.gen.isCheckmate(self.scene.board):
                self.scene.form.finished = time.clock()
                sol_time = self.scene.form.finished - self.scene.form.started
                c = self.scene.form.con
                pid = self.scene.form.posid
                c.execute("UPDATE STATS SET USER_TIME = (USER_TIME * USERS + ?) / (USERS + 1) WHERE PUZ_ID = ?", (sol_time, pid))
                c.commit()
                c.execute("UPDATE STATS SET USERS = USERS + 1 WHERE PUZ_ID = ?", (pid, ))
                c.commit()
                mb = QMessageBox()
                mb.setText("Checkmated! Time: %s sec." % sol_time)
                mb.exec()
        self.setCursor(Qt.OpenHandCursor)
        QGraphicsItem.mouseReleaseEvent(self, event)
        if moved:
            if self.scene.form.half_moves < self.scene.form.max_half_moves:
                if move in self.scene.form.sol[0]:
                    self.scene.doAIMove(random.choice(
                        list(self.scene.gen.generateMoves(self.scene.board))))
                elif move in self.scene.form.sol[1]:
                    self.scene.doAIMove(self.scene.form.sol[1][move])
            else:
                if not self.scene.gen.isCheckmate(self.scene.board):
                    mb = QMessageBox()
                    mb.setText(
                        "Sorry, solution is incorrect. Try another time.")
                    mb.exec()
                self.scene.form.mode = GameMode.Waiting

    def __str__(self):
        return "Piece: %s" % self.piece


class BoardScene(QGraphicsScene):

    def __init__(self, cellSize, form, board=None):
        QGraphicsScene.__init__(self)
        self.cellSize = cellSize
        if board is None:
            board = Chessboard()
        self.loadBoard(board)
        self.gen = MoveGenerator()
        self.form = form

    def loadBoard(self, board):
        self.board = board
        self.clear()
        offset = self.cellSize // 20
        scale = self.cellSize / 48
        for i in range(8):
            for j in range(8):
                piece = self.board.board[i][j]
                if piece != PIECE_Empty:
                    item = PieceItem(piece, self)
                    item.setPos(self.cellSize * i + offset,
                                self.cellSize * (7 - j) + offset)
                    item.setScale(scale)
                    self.addItem(item)

    def drawBackground(self, painter, rect):
        color = QColor(68, 34, 3)
        painter.setBrush(color)
        painter.setPen(color)
        for i in range(8):
            for j in range(8):
                if (i + j) % 2:
                    painter.drawRect(self.cellSize * i, self.cellSize * j,
                                     self.cellSize, self.cellSize)

    def doAIMove(self, move):
        print("moved %s" % move, flush=True)
        if self.board.white_to_move:
            movestr = "%d. %s-%s    " % (
                self.board.move_number, move[0:2], move[2:4])
        else:
            movestr = "%s-%s\n" % (move[0:2], move[2:4])
        self.form.moveView.insertPlainText(movestr)
        captured = self.board.move(move)
        self.form.half_moves += 1
        offset = self.cellSize // 20
        i, j = (ord(move[0]) - ord('a'), ord(move[1]) - ord('1'))
        k, l = (ord(move[2]) - ord('a'), ord(move[3]) - ord('1'))
        _from = QPointF(self.cellSize * i + offset,
                        self.cellSize * (7 - j) + offset)
        _to = QPointF(self.cellSize * k + offset,
                      self.cellSize * (7 - l) + offset)
        piece = self.itemAt(_from, QTransform())
        if captured:
            cappiece = self.itemAt(_to, QTransform())
            self.removeItem(cappiece)
        piece.setPos(_to)
        # TODO: Handle pawn promotion

    def mousePressEvent(self, event):
        QGraphicsScene.mousePressEvent(self, event)
        if self.form.mode == GameMode.Editing:
            btn = event.button()
            pos = event.scenePos()
            piece = self.itemAt(pos, QTransform())
            offset = self.cellSize // 20
            scale = self.cellSize / 48
            i = (int(pos.x()) - offset) // self.cellSize
            j = (int(pos.y()) - offset) // self.cellSize
            if not piece:
                item = PieceItem(PIECE_WhiteKing, self)
                item.setPos(self.cellSize * i + offset,
                            self.cellSize * j + offset)
                item.setScale(scale)
                self.addItem(item)
                self.board.board[i][7 - j] = PIECE_WhiteKing
            else:
                try:
                    if btn == 1:
                        newpiece = piece.piece + (-1, 1)[piece.piece > 0]
                    elif btn == 2:
                        newpiece = -piece.piece
                    else:
                        return
                    piece.setSharedRenderer(PieceItem.renderers[newpiece])
                    piece.piece = newpiece
                    self.board.board[i][7 - j] = newpiece
                except KeyError:
                    self.removeItem(piece)
                    self.board.board[i][7 - j] = PIECE_Empty


class SolutionGenerate(QThread):
    generationFinished = pyqtSignal(tuple)

    def __init__(self, board, mate_in, gen):
        QThread.__init__(self)
        self.board = board
        self.mate_in = mate_in
        self.gen = gen

    def __del__(self):
        self.wait()

    def run(self):
        sol = self.gen.generateSolution(self.board, self.mate_in)
        self.generationFinished.emit(sol)


class Form(QWidget):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.con = sqlite3.connect("pdb.sqlite")
        cellSize = 60
        # Buttons
        self.buttonLayout = QVBoxLayout()
        self.buttonNewPosition = QPushButton("New position")
        self.buttonLayout.addWidget(self.buttonNewPosition)
        self.buttonNewPosition.pressed.connect(self.onNewPosition)
        self.buttonLoadPosition = QPushButton("Load position")
        self.buttonLayout.addWidget(self.buttonLoadPosition)
        self.buttonLoadPosition.pressed.connect(self.onLoadPosition)
        self.buttonSavePosition = QPushButton("Save position to DB")
        self.buttonLayout.addWidget(self.buttonSavePosition)
        # self.buttonSavePosition.pressed.connect(self.onSavePosition)
        self.buttonVerify = QPushButton("Verify correctness")
        self.buttonLayout.addWidget(self.buttonVerify)
        # self.buttonVerify.pressed.connect(self.onVerify)
        self.buttonResign = QPushButton("Resign")
        self.buttonLayout.addWidget(self.buttonResign)
        self.buttonResign.pressed.connect(self.onResign)
        self.buttonToggleMode = QPushButton("Toggle Edit/Solve")
        self.buttonLayout.addWidget(self.buttonToggleMode)
        self.buttonToggleMode.pressed.connect(self.onToggleMode)
        # GroupBox
        self.groupBoxParams = QGroupBox("Parameters")
        self.buttonLayout.addWidget(self.groupBoxParams)
        paramsLayout = QVBoxLayout()
        self.radioButtonWhite = QRadioButton("White")
        paramsLayout.addWidget(self.radioButtonWhite)
        self.radioButtonBlack = QRadioButton("Black")
        paramsLayout.addWidget(self.radioButtonBlack)
        self.checkBoxCastleWK = QCheckBox("White kingside")
        paramsLayout.addWidget(self.checkBoxCastleWK)
        self.checkBoxCastleWQ = QCheckBox("White queenside")
        paramsLayout.addWidget(self.checkBoxCastleWQ)
        self.checkBoxCastleBK = QCheckBox("Black kingside")
        paramsLayout.addWidget(self.checkBoxCastleBK)
        self.checkBoxCastleBQ = QCheckBox("Black queenside")
        paramsLayout.addWidget(self.checkBoxCastleBQ)
        self.groupBoxParams.setLayout(paramsLayout)
        # Move view
        self.moveView = QTextEdit("Moves")
        self.moveView.setReadOnly(True)

        self.boardScene = BoardScene(cellSize, self)
        boardView = QGraphicsView(self.boardScene)
        boardView.setFixedSize(8 * cellSize + 4, 8 * cellSize + 4)
        boardView.setSceneRect(0, 0, 8 * cellSize, 8 * cellSize)

        mainLayout = QGridLayout()
        mainLayout.addLayout(self.buttonLayout, 0, 1)
        mainLayout.addWidget(boardView, 0, 0)
        mainLayout.addWidget(self.moveView, 0, 2)

        self.setLayout(mainLayout)
        self.setWindowTitle("ph8 application")

        self.mode = GameMode.Waiting

    @staticmethod
    def difficulty(time):
        return int(max(0, min((0.1 * time) ** 0.5, 10)))

    def onNewPosition(self):
        self.boardScene.loadBoard(Chessboard(empty=True))

    def onLoadPosition(self):
        cur = self.con.cursor()
        cur.execute("SELECT ID, COMMENT, MATE_IN, CPU_TIME, USER_TIME, USERS "
                    "FROM PUZ, STATS WHERE ID = PUZ_ID ORDER BY ID")
        pos_list = list(cur.fetchall())
        total_time = sum(p[4] * p[5] for p in pos_list)
        total_users = sum(p[5] for p in pos_list)
        coef_init = 25.0
        coef = total_time / total_users if total_users else coef_init
        diff = [Form.difficulty(p[4] if p[5] else coef * p[3])
                for p in pos_list]
        print(diff)
        pos_list = [(p[0], p[1], p[2], diff[i], p[5])
                    for (i, p) in enumerate(pos_list)]
        positions = ["#%s: %s (mate in %s; difficulty %s reported by %s users)" % p
                     for p in pos_list]
        pos, ok = QInputDialog.getItem(self, "Select a position",
                                       "Position list", positions, 0, False)
        if ok:
            # Retrieve the id
            posid = int(pos.split(":")[0][1:])
            cur.execute("SELECT * FROM PUZ WHERE ID = %d" % posid)
            p = cur.fetchone()
            cb = Chessboard()
            cb.parseFEN(p[1])
            self.boardScene.loadBoard(cb)
            self.radioButtonWhite.setChecked(cb.white_to_move)
            self.radioButtonBlack.setChecked(not cb.white_to_move)
            self.checkBoxCastleWK.setChecked("K" in cb.castling)
            self.checkBoxCastleWQ.setChecked("Q" in cb.castling)
            self.checkBoxCastleBK.setChecked("k" in cb.castling)
            self.checkBoxCastleBK.setChecked("q" in cb.castling)
            self.moveView.clear()
            self.half_moves = 0
            self.max_half_moves = 2 * int(p[2]) - 1
            if self.mode == GameMode.Waiting:
                self.mode = GameMode.Solving
                solThread = SolutionGenerate(
                    cb, int(p[2]), self.boardScene.gen)
                solThread.generationFinished.connect(self.solveStarted)
                solThread.start()
                self.posid = posid

    def onToggleMode(self):
        if self.mode == GameMode.Editing:
            self.mode = GameMode.Waiting
        elif self.mode == GameMode.Waiting:
            self.mode = GameMode.Editing

    def solveStarted(self, sol):
        self.sol = sol
        self.started = time.clock()

    def onResign(self):
        self.mode = GameMode.Waiting

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self._mode = value
        self.boardScene.mode = value
        if value == GameMode.Waiting:
            self.buttonNewPosition.setEnabled(False)
            self.buttonLoadPosition.setEnabled(True)
            self.buttonSavePosition.setEnabled(False)
            self.buttonVerify.setEnabled(False)
            self.buttonResign.setEnabled(False)
            self.buttonToggleMode.setEnabled(True)
            self.groupBoxParams.setEnabled(False)
            self.boardScene.loadBoard(Chessboard())
        elif value == GameMode.Solving:
            self.buttonNewPosition.setEnabled(False)
            self.buttonLoadPosition.setEnabled(False)
            self.buttonSavePosition.setEnabled(False)
            self.buttonVerify.setEnabled(False)
            self.buttonResign.setEnabled(True)
            self.buttonToggleMode.setEnabled(False)
            self.groupBoxParams.setEnabled(False)
        elif value == GameMode.Editing:
            self.buttonNewPosition.setEnabled(True)
            self.buttonLoadPosition.setEnabled(True)
            self.buttonSavePosition.setEnabled(True)
            self.buttonVerify.setEnabled(False)
            self.buttonResign.setEnabled(False)
            self.buttonToggleMode.setEnabled(True)
            self.groupBoxParams.setEnabled(True)
            self.radioButtonWhite.setChecked(True)
            self.radioButtonBlack.setChecked(False)
            self.checkBoxCastleWK.setChecked(True)
            self.checkBoxCastleWQ.setChecked(True)
            self.checkBoxCastleBK.setChecked(True)
            self.checkBoxCastleBQ.setChecked(True)
            self.boardScene.loadBoard(Chessboard(empty=True))


def resetdb(profile=False):
    f = open("test.log", "w")
    print("*** Running in test mode ***", file=f)
    con = sqlite3.connect('pdb.sqlite')
    n = list(con.cursor().execute("SELECT COUNT(*) FROM PUZ"))[0][0]
    con.cursor().execute("DELETE FROM STATS")
    con.cursor().execute("VACUUM")
    gen = MoveGenerator()
    for i in range(1, n + 1):
        print("Trying position %d..." % i, file=f)
        p = list(con.cursor().execute(
            "SELECT * FROM PUZ WHERE ID = %d" % i))[0]
        nmax = 1 if profile else 10
        t = 0
        for j in range(nmax):
            board = Chessboard()
            board.parseFEN(p[1])
            if p[2] == 2:
                time = gen.gsmi2TimedTest(board)[1]
                t += time
        print("AVERAGE TIME OF %d attempts: %f s" %
              (nmax, t / nmax), file=f)
        con.cursor().execute("INSERT INTO STATS(PUZ_ID, CPU_TIME, " +
                             "USER_TIME, USERS) VALUES (%s, %s, 0, 0)"
                             % (i, t / nmax))
        con.commit()
    f.close()
    con.close()


if __name__ == '__main__':
    import sys
    import cProfile
    argc = len(sys.argv)
    if argc == 1:
        app = QApplication(sys.argv)
        screen = Form()
        screen.show()
        sys.exit(app.exec_())
    elif argc >= 2 and sys.argv[1] == "resetdb":
        do_profiling = (argc >= 3 and sys.argv[2] == "profile")
        if do_profiling:
            cProfile.run("resetdb(True)")
        else:
            resetdb(True)
