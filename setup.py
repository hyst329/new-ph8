from distutils.core import setup
from Cython.Build import cythonize

setup(name='ph8',
      ext_modules=cythonize(["chessboard.py", "movegen.py"]))
