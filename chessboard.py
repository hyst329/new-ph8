PIECE_Empty = 0
PIECE_WhiteKing = 1
PIECE_WhiteQueen = 2
PIECE_WhiteBishop = 3
PIECE_WhiteKnight = 4
PIECE_WhiteRook = 5
PIECE_WhitePawn = 6
PIECE_BlackKing = -1
PIECE_BlackQueen = -2
PIECE_BlackBishop = -3
PIECE_BlackKnight = -4
PIECE_BlackRook = -5
PIECE_BlackPawn = -6

class Chessboard:

    def __init__(self, empty=False):
        self.board = [[]] * 8
        self.white_to_move = True
        for i in range(8):
            self.board[i] = ([PIECE_Empty] * 8).copy()
        if not empty:
            self.startingPosition()
        self.en_passant = None
        self.castling = set("KQkq")
        self.move_number = 0

    def startingPosition(self):
        self['e1'] = PIECE_WhiteKing
        self['d1'] = PIECE_WhiteQueen
        self['c1'] = self['f1'] = PIECE_WhiteBishop
        self['b1'] = self['g1'] = PIECE_WhiteKnight
        self['a1'] = self['h1'] = PIECE_WhiteRook
        self['e8'] = PIECE_BlackKing
        self['d8'] = PIECE_BlackQueen
        self['c8'] = self['f8'] = PIECE_BlackBishop
        self['b8'] = self['g8'] = PIECE_BlackKnight
        self['a8'] = self['h8'] = PIECE_BlackRook
        for i in range(8):
            self[chr(ord('a') + i) + '2'] = PIECE_WhitePawn
            self[chr(ord('a') + i) + '7'] = PIECE_BlackPawn
        self.white_to_move = True

    def parseFEN(self, fen):
        for i in range(8):
            self.board[i] = ([PIECE_Empty] * 8).copy()
        pieces, color, castle, en_passant, _, move = fen.split(' ')
        self.white_to_move = "bw".index(color)
        self.en_passant = en_passant if en_passant != "-" else None
        self.move_number = int(move) - 1
        self.castling = set(castle) if castle != '-' else set()
        ranks = pieces.split('/')
        piece_names = {'K': PIECE_WhiteKing, 'Q': PIECE_WhiteQueen,
                       'R': PIECE_WhiteRook, 'B': PIECE_WhiteBishop,
                       'N': PIECE_WhiteKnight, 'P': PIECE_WhitePawn,
                       'k': PIECE_BlackKing, 'q': PIECE_BlackQueen,
                       'r': PIECE_BlackRook, 'b': PIECE_BlackBishop,
                       'n': PIECE_BlackKnight, 'p': PIECE_BlackPawn}
        for r in range(8):
            rank = ranks[r]
            rindex = str(8 - r)
            findex = 1
            for c in rank:
                if c in piece_names:
                    self[chr(ord('a') + findex - 1) + rindex] = piece_names[c]
                    findex += 1
                else:
                    findex += int(c)

    def __getitem__(self, item):
        i = ord(item[0]) - ord('a')
        j = ord(item[1]) - ord('1')
        return self.board[i][j]

    def __setitem__(self, key, value):
        i = ord(key[0]) - ord('a')
        j = ord(key[1]) - ord('1')
        self.board[i][j] = value

    def isWhitePiece(self, square):
        return self[square] > 0

    def isBlackPiece(self, square):
        return self[square] < 0

    def findKing(self, white):
        king = PIECE_WhiteKing if white else PIECE_BlackKing
        i, j = next((i, j) for i in range(8) for j in range(8) if self.board[i][j] == king)
        return chr(ord('a') + i) + chr(ord('1') + j)

    def move(self, move):
        if not 4 <= len(move) <= 5:
            return
        start, dest = move[0:2], move[2:4]
        if start == "e1" and ("K" in self.castling or "Q" in self.castling):
            if "K" in self.castling:
                self.castling.remove("K")
            if "Q" in self.castling:
                self.castling.remove("Q")
            if dest == "g1":
                self["f1"] = PIECE_WhiteRook
                self["h1"] = PIECE_Empty
            if dest == "c1":
                self["d1"] = PIECE_WhiteRook
                self["a1"] = PIECE_Empty
        if start == "e8" and ("k" in self.castling or "q" in self.castling):
            if "k" in self.castling:
                self.castling.remove("k")
            if "q" in self.castling:
                self.castling.remove("q")
            if dest == "g8":
                self["f8"] = PIECE_BlackRook
                self["h8"] = PIECE_Empty
            if dest == "c8":
                self["d8"] = PIECE_BlackRook
                self["a8"] = PIECE_Empty
        if "h1" in (start, dest) and "K" in self.castling:
            self.castling.remove("K")
        if "a1" in (start, dest) and "Q" in self.castling:
            self.castling.remove("Q")
        if "h8" in (start, dest) and "k" in self.castling:
            self.castling.remove("k")
        if "a8" in (start, dest) and "q" in self.castling:
            self.castling.remove("q")

        en_passant_capture = ((self[start] in (PIECE_BlackPawn,
                                               PIECE_WhitePawn)) and
                              (dest == self.en_passant))

        if self[start] == PIECE_WhitePawn and \
                move[1] == '2' and move[3] == '4':
            self.en_passant = move[0] + '3'
        elif self[start] == PIECE_BlackPawn and \
                move[1] == '7' and move[3] == '5':
            self.en_passant = move[0] + '6'
        else:
            self.en_passant = None

        capture = self[dest] != PIECE_Empty

        self[dest] = self[start]
        self[start] = PIECE_Empty

        if en_passant_capture:
            pawn = move[2] + chr(ord(move[3]) + (1, -1)[self.white_to_move])
            self[pawn] = PIECE_Empty
            capture = True

        if self.white_to_move:
            self.move_number += 1
        if len(move) == 5:
            piece = move[4]
            value = ("__qbnr_".index(piece) * (-1, 1)[self.white_to_move])
            self[dest] = value
            # print(value)
        self.white_to_move = not self.white_to_move

        return capture
